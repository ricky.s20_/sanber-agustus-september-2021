1.	Soal 1 Membuat Database
	Create Database myshop;

2.	Soal 2 Membuat Table di Dalam Database
	Use myshop;
	
	CREATE TABLE users (
    id int AUTO_INCREMENT PRIMARY KEY ,
    name varchar(255),
    email varchar(255),
    password varchar(255)
	);
	
	CREATE TABLE categories (
    id int AUTO_INCREMENT PRIMARY KEY ,
    name varchar(255)
	);
	
	CREATE TABLE items (
    id int AUTO_INCREMENT PRIMARY KEY ,
    name varchar(255),
    description varchar(255),
    price int,
    stock int,
    category_id int,
    FOREIGN KEY (category_id) REFERENCES categories(id)
	);
	
3.	Soal 3 Memasukkan Data pada Table
	INSERT INTO users (name, email, password)
		VALUES ("John Doe","john@doe.com","john123"),
			   ("Jane Doe","jane@doe.com","jenita123");
	
	
	INSERT INTO categories (name)
	VALUES 	("gadget"),
			("cloth"),
			("men"),
			("women"),
			("branded");
			
	INSERT INTO items (name,description,price,stock,category_id)
		VALUES 	("Sumsang b50","hape keren dari merek sumsang","4000000","100","1"),
				("Uniklooh","baju keren dari brand ternama","500000","50","2"),
				("IMHO Watch","jam tangan anak yang jujur banget","2000000","10","1");

4.	Soal 4 Mengambil Data dari Database
	a. Mengambil data users
		SELECT id ,name,email FROM users;
		
	b. Mengambil data items
		( Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).)
		SELECT * FROM items WHERE price > 1000000;
		
		(Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).)
		SELECT * FROM items WHERE name LIKE '%uniklo%';
	
	c. Menampilkan data items join dengan kategori
		SELECT items.name,
			items.description,
			items.price,
			items.stock,			 
			items.category_id,
			categories.name as kategori
			FROM items 
			INNER JOIN categories ON items.category_id = categories.id;
			
5. 	Soal 5 Mengubah Data dari Database
	UPDATE items SET price = 2500000 WHERE name LIKE '%sumsang%';
