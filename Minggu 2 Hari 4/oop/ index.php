<?php
    require('Animal.php');
    require('Frog.php');
    require('Ape.php');

    $sheep = new Animal("shaun");

    echo "Name :$sheep->name<br>"; // "shaun"
    echo "Legs :$sheep->legs<br>"; // 4
    echo "Cold blooded :$sheep->cold_blooded<br><br>"; // "no"
    // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

    $kodok = new Frog("buduk");
    
    echo "Name : ".$kodok->name."<br>"; // "buduk"
    echo "Legs  : ".$kodok->legs."<br>"; // 4
    echo "Cold blooded : ".$kodok->cold_blooded."<br>" ; // false
    echo "Jump : ";
    echo $kodok->jump() ."<br><br>"; // "hop hop"

    $sungokong = new Ape("kera sakti");

    echo "Name : ".$sungokong->name."<br>"; // "kera sakti"
    echo "Legs  : ".$sungokong->legs."<br>"; // 2
    echo "Cold blooded  : ".$sungokong->cold_blooded ."<br>"; // false
    echo "Yell : ";
    echo $sungokong->yell()."<br><br>" ;// "Auooo"

?>
